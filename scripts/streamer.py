
import stream_processor
import sdp_dal.plasma as rpc
import pyarrow

import sys
import numpy

# Make sure we get enough parameters
if len(sys.argv) != 2:
    print("Usage: python demo_wstack.py /tmp/plasma")
    exit(1)
plasma_path = sys.argv[1]

# Connect
store = rpc.Store(plasma_path)
call = stream_processor.StreamCaller(store, broadcast=True, verbose=True)

# Allocate data
DATA_SIZE = 10000000
PARALLELISM = 5
data = numpy.arange(DATA_SIZE, dtype='float64')
print(f"Paylod size {memoryview(data).nbytes / 1e6} MB")

tag_refs = []
while True:

    # Wait for "ring buffer" tag to become available
    while len(tag_refs) >= PARALLELISM:
        refs = tag_refs.pop(0)
        for ref in refs:
            ref['tag'].get(0.1)

    # Make a call, transfering a new copy into the store
    call.find_processors(True)
    tag_refs.append(call.stream(data))
