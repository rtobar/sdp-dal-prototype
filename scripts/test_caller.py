
import sdp_dal.plasma as rpc

import pyarrow.plasma as plasma
import pyarrow

import numpy
import sys
import time
import abc

# Make sure we get enough parameters
if len(sys.argv) != 4:
    print("Usage: python test_caller.py /tmp/plasma 30303030 31303030")
    exit(1)
_, plasma_path, call_prefix, data_prefix = sys.argv

class TestCaller(rpc.Caller):
    def __init__(self, *args, **kwargs):
        self._register_call('scalar_add_tensor', [
            rpc.make_par('val', pyarrow.int32()),
            rpc.make_oid_input_par('inp'),
            rpc.make_oid_output_par('out')
        ])
        super(TestCaller, self).__init__(*args, **kwargs)

# Create access objects
call_oids = rpc.objectid_generator(rpc.parse_hex_objectid(call_prefix))
call = TestCaller(sys.argv[1], call_oids)
data_oids = rpc.objectid_generator(rpc.parse_hex_objectid(data_prefix))
store = rpc.Store(sys.argv[1], data_oids)

# Generate call record batches
ncalls = 1000
batch_calls = True
data = list([ next(data_oids) for _ in range(ncalls+1) ])
call_oids = []
if batch_calls:
    call_oids.append(call.scalar_add_tensor_batch([
        dict(val=i, inp=data[i], out=data[i+1])
        for i in range(ncalls)]))
else:
    for i in range(ncalls):
        call_oids.append(call.scalar_add_tensor(i, data[i], data[i+1]))

# Write numpy array as tensor for initial input
inp = numpy.arange(0, 100000, dtype=numpy.int32)
store.put_numpy(inp, oid=data[0])
start = time.time()
print("Wrote tensor {} to {}".format(
    inp, rpc.object_id_hex(data[0])))

# Wait for output
print("Waiting for result in {}...".format(rpc.object_id_hex(data[ncalls])))
out = store.get_numpy(plasma.ObjectID(data[ncalls]), pyarrow.int32()) # will block
duration = time.time() - start
print("Took {:.2f} s ({:.2f} ms/call)".format(duration, duration / ncalls * 1000))

# Clean up objects
call._client.delete([plasma.ObjectID(oid) for oid in data + call_oids])
