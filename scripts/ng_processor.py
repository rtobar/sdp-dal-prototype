
import sdp_dal.plasma as rpc
from sdp_dal.plasma.common import _numpy_cast_to_complex, _numpy_cast_from_complex

import nifty_gridder as ng

import pyarrow
from pyarrow import plasma

import sys

NIFTY_PROCS = [
    rpc.make_call_schema('ms2dirty', [
        rpc.make_tensor_input_par('uvw', pyarrow.float64(), ['t', 'uvw']),
        rpc.make_tensor_input_par('freq', pyarrow.float64(), ['ch']),
        rpc.make_tensor_input_par('ms', rpc.complex128, ['t','ch']),
        rpc.make_tensor_input_par('wgt', pyarrow.float64(), ['t','ch'], nullable=True),
        rpc.make_par('npix_x', pyarrow.int64()),
        rpc.make_par('npix_y', pyarrow.int64()),
        rpc.make_par('pixsize_x', pyarrow.float64()),
        rpc.make_par('pixsize_y', pyarrow.float64()),
        rpc.make_par('epsilon', pyarrow.float64()),
        rpc.make_par('do_wstacking', pyarrow.bool_(), nullable=True),
        rpc.make_par('nthreads', pyarrow.int64(), nullable=True),
        rpc.make_par('verbosity', pyarrow.int64(), nullable=True),
        rpc.make_tensor_output_par('output', pyarrow.float64(), ['x', 'y'])
    ]),
    rpc.make_call_schema('dirty2ms', [
        rpc.make_tensor_input_par('uvw', pyarrow.float64(), ['t', 'uvw']),
        rpc.make_tensor_input_par('freq', pyarrow.float64(), ['ch']),
        rpc.make_tensor_input_par('dirty', pyarrow.float64(), ['x', 'y']),
        rpc.make_tensor_input_par('wgt', pyarrow.float64(), ['t','ch'], nullable=True),
        rpc.make_par('pixsize_x', pyarrow.float64()),
        rpc.make_par('pixsize_y', pyarrow.float64()),
        rpc.make_par('epsilon', pyarrow.float64()),
        rpc.make_par('do_wstacking', pyarrow.bool_(), nullable=True),
        rpc.make_par('nthreads', pyarrow.int64(), nullable=True),
        rpc.make_par('verbosity', pyarrow.int64(), nullable=True),
        rpc.make_tensor_output_par('output', rpc.complex128, ['t','ch'])
    ])
]

class NiftyCaller(rpc.Caller):
    def __init__(self, *args, **kwargs):
        super(NiftyCaller, self).__init__(NIFTY_PROCS, *args, **kwargs)

class NiftyProcessor(rpc.Processor):

    def __init__(self, *args, **kwargs):
        super(NiftyProcessor, self).__init__(NIFTY_PROCS, *args, **kwargs)

    def _process_call(self, proc_name, batch):

        if proc_name == 'ms2dirty':
            return self._ms2dirty(batch)
        if proc_name == 'dirty2ms':
            return self._dirty2ms(batch)

        raise ValueError("Call to unknown procedure {}!".format(proc_name))

    def _ms2dirty(self, batch):

        # Read parameters
        uvw = self.tensor_parameter(batch, 'uvw', pyarrow.float64(), ['t', 'uvw'])
        freq = self.tensor_parameter(batch, 'freq', pyarrow.float64(), ['ch'])
        npix_x = self.parameter(batch, 'npix_x', pyarrow.int64())
        npix_y = self.parameter(batch, 'npix_y', pyarrow.int64())
        pixsize_x = self.parameter(batch, 'pixsize_x', pyarrow.float64())
        pixsize_y = self.parameter(batch, 'pixsize_y', pyarrow.float64())
        epsilon = self.parameter(batch, 'epsilon', pyarrow.float64())
        do_wstacking = self.parameter(batch, 'do_wstacking', pyarrow.bool_(), True)
        if do_wstacking is None:
            do_wstacking = False
        nthreads = self.parameter(batch, 'nthreads', pyarrow.int64(), True)
        if nthreads is None:
            nthreads = 1
        verbosity = self.parameter(batch, 'verbosity', pyarrow.int64(), True)
        if verbosity is None:
            verbosity = 0

        # Get ms tensor (don't know the type yet)
        ms = self.tensor_parameter(batch, 'ms', dim_names=['t','ch','complex'])
        ms = _numpy_cast_to_complex(ms)
        wgt = self.tensor_parameter(batch, 'wgt', dim_names=['t', 'ch'], allow_null=True)

        # Compute result
        res = ng.ms2dirty(uvw, freq, ms, wgt, npix_x, npix_y,
                          pixsize_x, pixsize_y, epsilon,
                          do_wstacking=do_wstacking,
                          nthreads=nthreads,
                          verbosity=verbosity)

        # Copy to storage
        self.output_tensor(batch, 'output', res)

    def _dirty2ms(self, batch):

        # Read parameters
        uvw = self.tensor_parameter(batch, 'uvw', pyarrow.float64(), ['t', 'uvw'])
        freq = self.tensor_parameter(batch, 'freq', pyarrow.float64(), ['ch'])
        pixsize_x = self.parameter(batch, 'pixsize_x', pyarrow.float64())
        pixsize_y = self.parameter(batch, 'pixsize_y', pyarrow.float64())
        epsilon = self.parameter(batch, 'epsilon', pyarrow.float64())
        do_wstacking = self.parameter(batch, 'do_wstacking', pyarrow.bool_(), True)
        if do_wstacking is None:
            do_wstacking = False
        nthreads = self.parameter(batch, 'nthreads', pyarrow.int64(), True)
        if nthreads is None:
            nthreads = 1
        verbosity = self.parameter(batch, 'verbosity', pyarrow.int64(), True)
        if verbosity is None:
            verbosity = 0
        output = self.oid_parameter(batch, 'output')

        # Get dirty tensor (don't know the type yet)
        dirty = self.tensor_parameter(batch, 'dirty', dim_names=['x','y'])
        wgt = self.tensor_parameter(batch, 'wgt', dim_names=['t','ch'], allow_null=True)

        # Compute result
        res = ng.dirty2ms(uvw, freq, dirty, wgt,
                          pixsize_x, pixsize_y, epsilon,
                          do_wstacking=do_wstacking,
                          nthreads=nthreads,
                          verbosity=verbosity)

        # Copy to storage
        self.output_tensor(batch, 'output', _numpy_cast_from_complex(res))

if __name__ == "__main__":

    if len(sys.argv) < 2:
        print("Please provide plasma socket path!")
        print("  Example: python processor.py /tmp/plasma")
        exit(1)

    # Create processor, process calls forever
    proc = NiftyProcessor(sys.argv[1])
    while True:
        try:
            proc.process()
        except KeyboardInterrupt:
            exit(0)
