# -*- coding: utf-8 -*-

from setuptools import setup, find_packages

setup(

    # Project name
    name="sdp-dal-prototype",

    # Code version
    version="0.1.1",

    # Authors
    author="Peter Wortmann, Matthieu Marseille",
    author_email="p.wortmann@skatelescope.org, matthieu.marseille@thales-services.fr",

    # Description
    description="SKA SDP data access library prototype",

    # URL
    url='http://gitlab.com/ska-telescope/sdp-dal-prototype',

    # Python version
    python_requires='>=3.6',

    # Licence
    license='Apache License Version 2.0',

    # Requirements
    install_requires=['numpy', 'pyarrow', 'tabulate', 'si-prefix', 'docopt'],

    # Packages
    packages=['sdp_dal', 'sdp_dal/plasma'],

    # Classifiers
    classifiers=[
        'Topic :: Database :: Front-Ends',
        'Topic :: Scientific/Engineering :: Astronomy',
        'Topic :: System :: Distributed Computing',
    ],

    # Command line tools
    scripts=['scripts/sdp-dump-plasma'],

    test_suite='test'

)
