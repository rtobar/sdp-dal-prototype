.. doctest-skip-all
.. _package-guide:

.. todo::
    - Still very proof-of-concept...

*********************************
SDP Data Access Library Prototype
*********************************

Some prototyping

sdp_dal.plasma.common
---------------------

.. automodule:: sdp_dal.plasma.common
    :members:
    :undoc-members:

sdp_dal.plasma.processor
------------------------

.. automodule:: sdp_dal.plasma.processor
    :members:
    :undoc-members:

sdp_dal.plasma.store
---------------------

.. automodule:: sdp_dal.plasma.store
    :members:
    :undoc-members:

sdp_dal.plasma.caller
---------------------

.. automodule:: sdp_dal.plasma.caller
    :members:
    :undoc-members:
