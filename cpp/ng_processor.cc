
#include "gridder_cxx.h"
#include "processor.h"

using namespace std;
using namespace gridder;
using gridder::detail::idx_t;

const std::vector<std::shared_ptr<arrow::Schema> > NIFTY_PROCS = {
    MakeCallSchema("ms2dirty", {
        MakeOIDInputPar("uvw"),
        MakeOIDInputPar("freq"),
        MakeOIDInputPar("ms"),
        MakeOIDInputPar("wgt", true),
        MakePar("npix_x", arrow::int64()),
        MakePar("npix_y", arrow::int64()),
        MakePar("pixsize_x", arrow::float64()),
        MakePar("pixsize_y", arrow::float64()),
        MakePar("epsilon", arrow::float64()),
        MakePar("do_wstacking", arrow::boolean(), true),
        MakePar("nthreads", arrow::int64(), true),
        MakePar("verbosity", arrow::int64(), true),
        MakeOIDOutputPar("output"),
    }),
    MakeCallSchema("dirty2ms", {
        MakeOIDInputPar("uvw"),
        MakeOIDInputPar("freq"),
        MakeOIDInputPar("dirty"),
        MakeOIDInputPar("wgt", true),
        MakePar("pixsize_x", arrow::float64()),
        MakePar("pixsize_y", arrow::float64()),
        MakePar("epsilon", arrow::float64()),
        MakePar("do_wstacking", arrow::boolean(), true),
        MakePar("nthreads", arrow::int64(), true),
        MakePar("verbosity", arrow::int64(), true),
        MakeOIDOutputPar("output"),
    })
};

class NiftyProcessor : public Processor {
public:
    NiftyProcessor() : Processor("NiftyProcessor", NIFTY_PROCS) {}
 protected:
    virtual arrow::Status ProcessCall(
        const std::string &proc_func,
        std::shared_ptr<arrow::RecordBatch> batch) override;

 private:
    arrow::Status ms2dirty(std::shared_ptr<arrow::RecordBatch> batch);
    arrow::Status dirty2ms(std::shared_ptr<arrow::RecordBatch> batch);
};

// Helpers for converting from Arrow's tensor types to mav. Can likely
// be generalised in some way...
template<size_t ndim, typename TYPE>
const_mav<typename TYPE::c_type,ndim>
make_const_mav(std::shared_ptr<arrow::NumericTensor<TYPE> > &in)
{
    using T = typename TYPE::c_type;
    myassert(ndim==in->ndim(), "dimension mismatch");
    array<size_t,ndim> dims; array<ptrdiff_t,ndim> str;
    for (size_t i=0; i<ndim; ++i) {
        dims[i]=in->shape()[i];
        str[i]=in->strides()[i]/sizeof(T);
        myassert(str[i]*ptrdiff_t(sizeof(T))==in->strides()[i], "weird strides");
    }
    return const_mav<T, ndim>(reinterpret_cast<const T *>(in->raw_data()), dims,str);
}

template<size_t ndim, typename TYPE>
mav<typename TYPE::c_type,ndim>
make_mav(std::shared_ptr<arrow::NumericTensor<TYPE> > &in)
{
    using T = typename TYPE::c_type;
    myassert(ndim==in->ndim(), "dimension mismatch");
    array<size_t,ndim> dims; array<ptrdiff_t,ndim> str;
    for (size_t i=0; i<ndim; ++i) {
        dims[i]=in->shape()[i];
        str[i]=in->strides()[i]/sizeof(T);
        myassert(str[i]*ptrdiff_t(sizeof(T))==in->strides()[i], "weird strides");
    }
    return mav<T, ndim>(reinterpret_cast<T *>(in->raw_mutable_data()), dims,str);
}

template<size_t ndim, typename TYPE>
const_mav<std::complex<typename TYPE::c_type>,ndim>
make_const_mav_cpx(std::shared_ptr<arrow::NumericTensor<TYPE> > &in)
{
    using T = typename TYPE::c_type; using C = std::complex<T>;
    myassert(ndim+1==in->ndim(), "dimension mismatch");
    myassert(in->shape()[ndim] == 2, "complex dimension shape not 2!");
    myassert(in->strides()[ndim] == ptrdiff_t(sizeof(T)), "weird complex dimension stride!");
    array<size_t,ndim> dims; array<ptrdiff_t,ndim> str;
    for (size_t i=0; i<ndim; ++i) {
        dims[i]=in->shape()[i];
        str[i]=in->strides()[i]/sizeof(T)/2;
        myassert(str[i]*2*ptrdiff_t(sizeof(T))==in->strides()[i], "weird strides");
    }
    return const_mav<C, ndim>(reinterpret_cast<const C *>(in->raw_data()),
                              dims,str);
}

template<size_t ndim, typename TYPE>
mav<std::complex<typename TYPE::c_type>,ndim>
make_mav_cpx(std::shared_ptr<arrow::NumericTensor<TYPE> > &in)
{
    using T = typename TYPE::c_type; using C = std::complex<T>;
    myassert(ndim+1==in->ndim(), "dimension mismatch");
    myassert(in->shape()[ndim] == 2, "complex dimension shape not 2!");
    myassert(in->strides()[ndim] == ptrdiff_t(sizeof(T)), "weird complex dimension stride!");
    array<size_t,ndim> dims; array<ptrdiff_t,ndim> str;
    for (size_t i=0; i<ndim; ++i) {
        dims[i]=in->shape()[i];
        str[i]=in->strides()[i]/sizeof(T)/2;
        myassert(str[i]*2*ptrdiff_t(sizeof(T))==in->strides()[i], "weird strides");
    }
    return mav<C, ndim>(reinterpret_cast<C *>(in->raw_mutable_data()),
                        dims,str);
}

arrow::Status NiftyProcessor::ProcessCall(const std::string &proc_func,
                                         std::shared_ptr<arrow::RecordBatch> batch)
{

    // Note that all of this would be *much* easier if we just had two
    // function variants for float and double, but what's the fun in that?
    if (proc_func == "ms2dirty") {
        return ms2dirty(batch);
    }
    if (proc_func == "dirty2ms") {
        return dirty2ms(batch);
    }

    return arrow::Status::NotImplemented("Unknown function ", proc_func);
}

arrow::Status NiftyProcessor::ms2dirty(std::shared_ptr<arrow::RecordBatch> batch)
{
    // Read parameters
    ARROW_ASSIGN_OR_RAISE(auto uvw_t, TensorParameter<arrow::DoubleType>(batch, "uvw"));
    ARROW_ASSIGN_OR_RAISE(auto freq_t, TensorParameter<arrow::DoubleType>(batch, "freq"));
    ARROW_ASSIGN_OR_RAISE(auto npix_x, NumericParameter<arrow::Int64Type>(batch, "npix_x"));
    ARROW_ASSIGN_OR_RAISE(auto npix_y, NumericParameter<arrow::Int64Type>(batch, "npix_y"));
    ARROW_ASSIGN_OR_RAISE(auto pixsize_x, NumericParameter<arrow::DoubleType>(batch, "pixsize_x"));
    ARROW_ASSIGN_OR_RAISE(auto pixsize_y, NumericParameter<arrow::DoubleType>(batch, "pixsize_y"));
    ARROW_ASSIGN_OR_RAISE(auto epsilon, NumericParameter<arrow::DoubleType>(batch, "epsilon"));
    ARROW_ASSIGN_OR_RAISE(auto do_wstacking, Optional(NumericParameter<arrow::BooleanType>(batch, "do_wstacking"),
                                                      false));
    ARROW_ASSIGN_OR_RAISE(auto nthreads, Optional(NumericParameter<arrow::Int64Type>(batch, "nthreads"),
                                                  1L));
    ARROW_ASSIGN_OR_RAISE(auto verbosity, Optional(NumericParameter<arrow::Int64Type>(batch, "verbosity"),
                                                   0L));
    
    // Package as nifty's vectors
    auto uvw = make_const_mav<2>(uvw_t);
    auto freq = make_const_mav<1>(freq_t);

    // Constant?
    const int nu = 2*npix_x, nv = 2*npix_y;

    // Determine type of visibilities
    ARROW_ASSIGN_OR_RAISE(auto ms_t, AnyTensorParameter(batch, "ms"));
    if (ms_t->type_id() == arrow::Type::DOUBLE) {

        // Convert to (complex) mav
        ARROW_ASSIGN_OR_RAISE(auto ms_tt, ToNumericTensor<arrow::DoubleType>(ms_t));
        auto ms = make_const_mav_cpx<2>(ms_tt);

        // Read (optional) weights
        ARROW_ASSIGN_OR_RAISE(auto default_wgt,
                              arrow::NumericTensor<arrow::DoubleType>::Make(arrow::Buffer::Wrap("", 0),
                                                                            {0,0}));
        ARROW_ASSIGN_OR_RAISE(auto wgt_t, Optional(TensorParameter<arrow::DoubleType>(batch, "wgt"), default_wgt));
        auto wgt = make_const_mav<2>(wgt_t);

        // Allocate output buffer (in Plasma)
        std::vector<int64_t> shape = { npix_x, npix_y };
        std::vector<std::string> dim_names = { "x", "y" };
        ARROW_ASSIGN_OR_RAISE(auto out_t, InplaceTensorOutput<arrow::DoubleType>(batch, "output",
                                                                                 shape, {}, dim_names));
        auto out = make_mav<2>(out_t);

        // Write out result
        gridder::detail::ms2dirty_general(uvw, freq, ms, wgt, pixsize_x, pixsize_y,
                                          nu, nv, epsilon, do_wstacking, nthreads, out, verbosity);
        
        // Seal
        ARROW_RETURN_NOT_OK(SealInplaceTensorOutput(batch, "output"));

    } else {
        return arrow::Status::NotImplemented("Data type not supported for ms2dirty", ms_t->type());
    }
    
    return arrow::Status::OK();
}

arrow::Status NiftyProcessor::dirty2ms(std::shared_ptr<arrow::RecordBatch> batch)
{
    // Read parameters
    ARROW_ASSIGN_OR_RAISE(auto uvw_t, TensorParameter<arrow::DoubleType>(batch, "uvw"));
    ARROW_ASSIGN_OR_RAISE(auto freq_t, TensorParameter<arrow::DoubleType>(batch, "freq"));
    ARROW_ASSIGN_OR_RAISE(auto pixsize_x, NumericParameter<arrow::DoubleType>(batch, "pixsize_x"));
    ARROW_ASSIGN_OR_RAISE(auto pixsize_y, NumericParameter<arrow::DoubleType>(batch, "pixsize_y"));
    ARROW_ASSIGN_OR_RAISE(auto epsilon, NumericParameter<arrow::DoubleType>(batch, "epsilon"));
    ARROW_ASSIGN_OR_RAISE(auto do_wstacking, Optional(NumericParameter<arrow::BooleanType>(batch, "do_wstacking"),
                                                      false));
    ARROW_ASSIGN_OR_RAISE(auto nthreads, Optional(NumericParameter<arrow::Int64Type>(batch, "nthreads"),
                                                  1L));
    ARROW_ASSIGN_OR_RAISE(auto verbosity, Optional(NumericParameter<arrow::Int64Type>(batch, "verbosity"),
                                                   0L));
    
    // Convert parameters
    auto uvw = make_const_mav<2>(uvw_t);
    auto freq = make_const_mav<1>(freq_t);

    // Determine type of visibilities
    ARROW_ASSIGN_OR_RAISE(auto dirty_t, AnyTensorParameter(batch, "dirty"));
    const int nu = 2*dirty_t->shape()[0], nv = 2*dirty_t->shape()[1];
    if (dirty_t->type_id() == arrow::Type::DOUBLE) {

        // Convert to mav
        ARROW_ASSIGN_OR_RAISE(auto dirty_tt, ToNumericTensor<arrow::DoubleType>(dirty_t));
        auto dirty = make_const_mav<2>(dirty_tt);

        // Read (optional) weights
        ARROW_ASSIGN_OR_RAISE(auto default_wgt,
                              arrow::NumericTensor<arrow::DoubleType>::Make(arrow::Buffer::Wrap("", 0),
                                                                            {0,0}));
        ARROW_ASSIGN_OR_RAISE(auto wgt_t, Optional(TensorParameter<arrow::DoubleType>(batch, "wgt"), default_wgt));
        auto wgt = make_const_mav<2>(wgt_t);

        // Allocate output buffer (in Plasma)
        std::vector<int64_t> shape = { uvw_t->shape()[0], freq_t->shape()[0], 2 };
        std::vector<std::string> dim_names = { "baseline", "channel", "complex" };
        ARROW_ASSIGN_OR_RAISE(auto out_t, InplaceTensorOutput<arrow::DoubleType>(batch, "output",
                                                                                 shape, {}, dim_names));
        auto out = make_mav_cpx<2>(out_t);

        // Write out result
        gridder::detail::dirty2ms_general<double>(uvw, freq, dirty, wgt, pixsize_x, pixsize_y,
                                                  nu, nv, epsilon, do_wstacking, nthreads, out, verbosity);

        // Seal
        ARROW_RETURN_NOT_OK(SealInplaceTensorOutput(batch, "output"));

    } else {
        return arrow::Status::NotImplemented("Data type not supported for dirty2ms", dirty_t->type());
    }
    
    return arrow::Status::OK();
}

int main(int argc, const char* argv[])
{
    return ProcessorMain<NiftyProcessor>(argc, argv);
}
