#include "processor.h"

#include <arrow/ipc/writer.h>
#include <arrow/builder.h>
#include <iostream>
#include <unordered_set>
#include <string_view>
#include <unistd.h>

arrow::Result<std::string> static
MakeNamespaceDecl(const std::string &name,
		  const std::vector<std::shared_ptr<arrow::Schema> > &procs)
{

    // Collect names + serialise schemas
    std::vector<std::string> proc_names, proc_schemas;
    for (auto proc : procs) {

	// Get proc name
	auto meta = proc->metadata();
	if (!meta) meta = std::make_shared<arrow::KeyValueMetadata>();
	ARROW_ASSIGN_OR_RAISE(std::string proc_name, meta->Get(proc_func_meta));

	// Serialise schema
	ARROW_ASSIGN_OR_RAISE(auto schema, arrow::ipc::SerializeSchema(*proc, nullptr));

	// Collect data
	proc_names.push_back(proc_name);
	proc_schemas.push_back(schema->ToString());
    }

    // Make batch record
    arrow::StringBuilder builder; arrow::BinaryBuilder builder2;
    builder.AppendValues(proc_names);
    builder2.AppendValues(proc_schemas);
    std::shared_ptr<arrow::StringArray> proc_names_arr;
    std::shared_ptr<arrow::BinaryArray> proc_schemas_arr;
    builder.Finish(&proc_names_arr);
    builder2.Finish(&proc_schemas_arr);
    auto batch = arrow::RecordBatch::Make(PROCS_SCHEMA, procs.size(), {
	    proc_names_arr, proc_schemas_arr });

    // Create new schema with appropriate metadata
    const auto schema = PROCS_SCHEMA->WithMetadata(std::make_shared<arrow::KeyValueMetadata>(
	std::vector{PROC_NAMESPACE_META, PROC_NAMESPACE_PID_META},
	std::vector{name, std::to_string(getpid())}));

    // And serialise in turn
    ARROW_ASSIGN_OR_RAISE(auto stream, arrow::io::BufferOutputStream::Create());
    ARROW_ASSIGN_OR_RAISE(auto writer, arrow::ipc::NewStreamWriter(&*stream, schema));
    writer->WriteRecordBatch(*arrow::RecordBatch::Make(
    			 PROCS_SCHEMA, procs.size(), { proc_names_arr, proc_schemas_arr }));
    writer->Close();
    ARROW_ASSIGN_OR_RAISE(auto buf, stream->Finish());
    return buf->ToString();
}

std::string_view GetNamespacePrefix(const plasma::ObjectID &oid)
{
    return std::string_view(reinterpret_cast<const char *>(oid.data()),
			    NAMESPACE_ID_SIZE);
}

arrow::Result<std::pair<plasma::ObjectID, std::shared_ptr<arrow::Buffer> > >
ReserveNamespace(std::shared_ptr<plasma::PlasmaClient> client,
		 const std::string &name,
		 const std::vector<std::shared_ptr<arrow::Schema> > &procs,
		 const plasma::ObjectTable *obj_table)
{

    // Collect used prefixes
    std::unordered_set<std::string_view> used_prefixes;
    if (obj_table) {
	for (const auto &entry : *obj_table) {
	    used_prefixes.insert(GetNamespacePrefix(entry.first));
	}
    }

    // Serialise namespace declaration
    ARROW_ASSIGN_OR_RAISE(auto decl, MakeNamespaceDecl(name, procs));

    // Find a free namespace
    std::string oid_str(plasma::kUniqueIDSize, '\0');
    std::string_view prefix(&oid_str[0], NAMESPACE_ID_SIZE);
    while(true) {

	// Check whether prefix is free
	if (!used_prefixes.count(prefix)) {

	    // Attempt to write reservation
	    std::shared_ptr<arrow::Buffer> buf;
	    auto oid = plasma::ObjectID::from_binary(oid_str);
	    auto result = client->Create(oid, decl.size(), nullptr, 0, &buf);
	    if (result.ok()) {

		// Copy data
		memcpy(buf->mutable_data(), &decl[0], decl.size());

		// Seal & delete (after that point the Plasma object is only
		// kept alive by buffer reference, which we return)
		ARROW_RETURN_NOT_OK(client->Seal(oid));
		ARROW_RETURN_NOT_OK(client->Delete(oid));

		return std::make_pair(oid, buf);

	    // Errors that are *not* a collision are fatal
	    } else if (!plasma::IsPlasmaObjectExists(result)) {
		return result;
	    }
	}

	// Next ID
	int i;
	for (i = 0; i < NAMESPACE_ID_SIZE; i++) {
	    if (oid_str[NAMESPACE_ID_SIZE-i-1] == 255) {
		oid_str[NAMESPACE_ID_SIZE-i-1] = 0;
	    } else {
		oid_str[NAMESPACE_ID_SIZE-i-1]++;
		break;
	    }
	}
	if (i >= NAMESPACE_ID_SIZE) {
	    return arrow::Status::CapacityError("Failed to locate free namespace!");
	}
    }



}

Processor::~Processor()
{
}

arrow::Status Processor::Connect(const std::string &plasma_path)
{

    // Create connection
    auto client = std::make_shared<plasma::PlasmaClient>();
    ARROW_RETURN_NOT_OK(client->Connect(plasma_path));

    // Subscribe to object updates
    int sub_fd;
    ARROW_RETURN_NOT_OK(client->Subscribe(&sub_fd));

    // Get initial object list
    ARROW_RETURN_NOT_OK(client->List(&obj_table_));

    // Reserve a name space
    ARROW_ASSIGN_OR_RAISE(auto prefix_root,
			  ReserveNamespace(client, name_, procs_, &obj_table_));

    // Set
    client_ = client;
    sub_fd_ = sub_fd;
    prefix_ = prefix_root.first;
    prefix_length_ = NAMESPACE_ID_SIZE;
    root_ = prefix_root.second;
    return arrow::Status::OK();
}

arrow::Status Processor::Process()
{

    // Get next notification
    plasma::ObjectID oid;
    auto entry = std::unique_ptr<plasma::ObjectTableEntry>(
	new plasma::ObjectTableEntry);
    ARROW_RETURN_NOT_OK(client_->GetNotification(
	sub_fd_, &oid, &entry->data_size, &entry->metadata_size));

    // Deletion?
    if (entry->data_size < 0) {
	obj_table_.erase(oid);
	return arrow::Status::OK();
    }
    assert(obj_table_.count(oid) == 0);

    // Set in table
    //std::cout << oid.hex() << " created (" << entry->data_size << " + "
    //          << entry->metadata_size << " bytes)" << std::endl;
    obj_table_[oid] = std::move(entry);

    // Has prefix? Attempt to read as calls
    if (memcmp(oid.data(), prefix_.data(), prefix_length_) == 0) {
	if (!IsNamespaceDecl(oid)) {
	    ARROW_RETURN_NOT_OK(ReadCalls(oid));
	}
    }

    // Calls waiting for this input object?
    auto range = delayed_calls_.equal_range(oid);
    for (auto it = range.first; it != range.second; ++it) {
	std::vector<int> in_obj_cols, out_obj_cols;
	auto call_oid = it->second.first; auto batch = it->second.second;
	FindInOutPars(batch->schema(), &in_obj_cols, &out_obj_cols);
	ARROW_RETURN_NOT_OK(ReadCall(call_oid, batch, in_obj_cols, out_obj_cols));
    }
    delayed_calls_.erase(oid);

    return arrow::Status::OK();
}

int ParseHexObjectID(const std::string &str, plasma::ObjectID *prefix)
{

    // Validate
    if (str.length() % 2 != 0)
	return 0;
    if (str.length() / 2 > plasma::kUniqueIDSize)
	return 0;

    // Read bytes
    for (size_t i = 0; i < str.length() / 2; i++) {
	std::stringstream strm(str.substr(i*2, 2));
	int num;
	strm >> std::hex >> num;
	prefix->mutable_data()[i] = num;
    }

    return str.length() / 2;
}

void Processor::FindInOutPars(std::shared_ptr<arrow::Schema> schema,
			      std::vector<int> *in_obj_cols,
			      std::vector<int> *out_obj_cols)
{
    // Find input/output parameters
    for (int col = 0; col < schema->num_fields(); col++) {
	auto field = schema->field(col);

	// Check that field has Object ID type and has parameter kind
	// metadata associated with it
	int meta;
	if (!field->HasMetadata() ||
	    !field->type()->Equals(OBJECT_ID_TYPE) ||
	    (meta = field->metadata()->FindKey(proc_par_meta)) < 0) continue;

	// Check parameter kind, add to appropriate list
	auto parkind = field->metadata()->value(meta);
	if (parkind == proc_par_meta_in) {
	    in_obj_cols->push_back(col);
	}
	if (parkind == proc_par_meta_out) {
	    out_obj_cols->push_back(col);
	}
    }
}

arrow::Status Processor::ReadCalls(const plasma::ObjectID &object_id)
{

    std::cout << "Processing " << object_id.hex() << std::endl;

    std::vector<plasma::ObjectBuffer> object_buffers;
    ARROW_RETURN_NOT_OK(client_->Get({object_id}, -1, &object_buffers));

    // Try to open stream for reading record batches. This will read
    // the schema from the prefix, which tells us the function to call
    // as well as parameter schemas.
    arrow::io::BufferReader reader(object_buffers[0].data);
    ARROW_ASSIGN_OR_RAISE(auto batch_reader,
			  arrow::ipc::RecordBatchStreamReader::Open(&reader));

    // Identify in/out object parameters
    std::vector<int> in_obj_cols, out_obj_cols;
    FindInOutPars(batch_reader->schema(), &in_obj_cols, &out_obj_cols);

    // Read function call record batch(es)
    while(true) {

	// At end of stream? Need to check this manually because
	// apparently ReadNext just segfaults otherwise... There must
	// be a more elegant way to do this?
	const auto peek = reader.Peek(8);
	const auto end_marker = std::string("\377\377\377\377\0\0\0\0", 8);
	if (!peek.ok() || *peek == end_marker) break;

	// Read next record batch
	std::shared_ptr<arrow::RecordBatch> batch;
	ARROW_RETURN_NOT_OK(batch_reader->ReadNext(&batch));

	// Go through rows (i.e. individual calls)
	for (int row = 0; row < batch->num_rows(); row++) {
	    std::shared_ptr<arrow::RecordBatch> batch_row = batch->Slice(row);
	    ARROW_RETURN_NOT_OK(ReadCall(object_id, batch_row,
					 in_obj_cols, out_obj_cols));
	}
    }
    return arrow::Status::OK();
}

arrow::Result<plasma::ObjectID>
Processor::OIDParameter(std::shared_ptr<arrow::Array> arr,
			const std::string &par_name)
{
    ARROW_ASSIGN_OR_RAISE(auto oid_str,
			  (Parameter<arrow::FixedSizeBinaryType, arrow::util::string_view>(arr, par_name)));
    return plasma::ObjectID::from_binary(std::string(oid_str));
}

template <class T>
arrow::Result<std::string> ReadMetadata(std::shared_ptr<T> schema,
					std::string key)
{
    // Check key exists in metadata
    int i;
    if (!schema->HasMetadata() || (i = schema->metadata()->FindKey(key)) < 0) {
	arrow::Status status(arrow::StatusCode::TypeError,
			     "Expected meta data " + key + " not found!");
	return arrow::Result<std::string>(status);
    }
    // Return
    return arrow::Result<std::string>(schema->metadata()->value(i));
}

template <class T>
std::string ReadMetadata(std::shared_ptr<T> schema,
			 std::string key, std::string default_)
{
    // Check key exists in metadata
    int i;
    if (!schema->HasMetadata() || (i = schema->metadata()->FindKey(key)) < 0) {
	return default_;
    }
    // Return
    return schema->metadata()->value(i);
}

arrow::Status Processor::ReadCall(const plasma::ObjectID &call_oid,
				  std::shared_ptr<arrow::RecordBatch> batch,
				  const std::vector<int> &in_obj_cols,
				  const std::vector<int> &out_obj_cols)
{

    // Get function name
    ARROW_ASSIGN_OR_RAISE(std::string proc_func,
			  ReadMetadata(batch->schema(), proc_func_meta))

    // Check that outputs are not satisfied (i.e. that this
    // request has not been processed already)
    bool already_processed = true;
    auto field_names = batch->schema()->field_names();
    for (int col : out_obj_cols) {
	ARROW_ASSIGN_OR_RAISE(auto oid, OIDParameter(batch->column(col), field_names[col]));
	if (obj_table_.find(oid) == obj_table_.end()) {
	    already_processed = false;
	    break;
	}
    }
    if (already_processed) {
	std::cout << "Outputs for " << proc_func << " already sealed, ignoring."
		  << std::endl;
	return arrow::Status::OK();
    }

    // Check that inputs are present
    for (int col : in_obj_cols) {
	// Read object ID. Ignore nulls.
	auto oid_res = OIDParameter(batch->column(col), field_names[col]);
	if (oid_res.status().IsIndexError()) continue;
	ARROW_ASSIGN_OR_RAISE(auto oid, oid_res);
	// Not present? Delay
	if (obj_table_.find(oid) == obj_table_.end()) {
	    //std::cout << "Inputs for " << proc_func << " missing, delaying."
	    //          << std::endl;
	    // Add to delayed calls, increase ref count on call plasma object
	    delayed_calls_.insert(std::make_pair(oid,std::make_pair(call_oid,
								    batch)));
	    return arrow::Status::OK();
	}
    }

    // Process the call
    //std::cout << "Processing call to " << proc_func << "..." << std::endl;
    ARROW_RETURN_NOT_OK(ProcessCall(proc_func, batch));

    return arrow::Status::OK();
}

arrow::Result<std::shared_ptr<arrow::Buffer> >
Processor::ObjectParameter(std::shared_ptr<arrow::RecordBatch> batch,
			   const std::string &par_name)
{

    // Make sure column exists and has right kind
    auto field = batch->schema()->GetFieldByName(par_name);
    ARROW_RETURN_IF(!field, arrow::Status::KeyError("Field '", par_name, "' not found!"));
    ARROW_ASSIGN_OR_RAISE(auto par, ReadMetadata(field, proc_par_meta))
    ARROW_RETURN_IF(par != proc_par_meta_in,
		    arrow::Status(arrow::StatusCode::Invalid,
				  "Field '" + par_name + "' not declared as input!"));

    // Read
    ARROW_ASSIGN_OR_RAISE(auto oid, OIDParameter(batch->GetColumnByName(par_name), par_name));
    std::vector<plasma::ObjectBuffer> object_buffers;
    ARROW_RETURN_NOT_OK(client_->Get({oid}, 0, &object_buffers));

    // Ensure object was found
    ARROW_RETURN_IF(!object_buffers[0].data,
		    arrow::Status::KeyError("Object '", oid.hex(), "' for field '", par_name, "' not found!"));

    // Return
    return std::move(object_buffers[0].data);
}

arrow::Result<std::shared_ptr<arrow::Tensor> >
Processor::AnyTensorParameter(std::shared_ptr<arrow::RecordBatch> batch,
			      const std::string &par_name)
{

    // Get buffer
    ARROW_ASSIGN_OR_RAISE(auto data, ObjectParameter(batch, par_name));

    // Read tensor
    arrow::io::BufferReader reader(data);
    return arrow::ipc::ReadTensor(&reader);
}

arrow::Status Processor::TensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
				      const std::string &par_name,
				      const arrow::Tensor &tensor)
{
    // Determine object ID of output
    ARROW_ASSIGN_OR_RAISE(auto oid_out, OIDParameter(batch->GetColumnByName(par_name), par_name));

    // Determine size including IPC headers
    int64_t tensor_size;
    ARROW_RETURN_NOT_OK(arrow::ipc::GetTensorSize(tensor, &tensor_size));

    // Allocate in Plasma
    std::shared_ptr<arrow::Buffer> out_buf;
    ARROW_RETURN_NOT_OK(client_->Create(oid_out, tensor_size, NULL, 0,
					&out_buf));

    // Write out
    arrow::io::FixedSizeBufferWriter writer(out_buf);
    int32_t metadata_length; int64_t tensor_size_written;
    ARROW_RETURN_NOT_OK(arrow::ipc::WriteTensor(tensor, &writer,
						&metadata_length,
						&tensor_size_written));

    // Finally seal
    ARROW_RETURN_NOT_OK(client_->Seal(oid_out));

    // Release the output object manually. Not sure why this is required...
    return client_->Release(oid_out);
}

arrow::Result<std::shared_ptr<arrow::Tensor> >
Processor::InplaceAnyTensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
				  const std::string &par_name,
				  const std::shared_ptr<arrow::FixedWidthType>& type,
				  const std::vector<int64_t>& shape,
				  const std::vector<int64_t>& strides,
				  const std::vector<std::string>& dim_names)
{

    // Determine object ID of output
    ARROW_ASSIGN_OR_RAISE(auto oid_out, OIDParameter(batch->GetColumnByName(par_name), par_name));

    // Allocate dummy buffer (TODO: not actually used, at some point
    // we should be able to just skip this, see below)
    int64_t size = type->bit_width() / 8;
    if (strides.size() == 0) {
	for (int64_t s : shape) size *= s;
    } else {
	ARROW_RETURN_IF(strides.size() != shape.size(),
			arrow::Status::Invalid("Size of strides must match shapes!"));
	size *= strides[strides.size()-1] * shape[shape.size()-1];
    }
    ARROW_ASSIGN_OR_RAISE(std::shared_ptr<arrow::Buffer> buf, arrow::AllocateBuffer(size));

    // Make a tensor out of it
    ARROW_ASSIGN_OR_RAISE(auto tensor, arrow::Tensor::Make(type, buf, shape, strides, dim_names));

    // Determine size including IPC headers
    int64_t tensor_size;
    ARROW_RETURN_NOT_OK(arrow::ipc::GetTensorSize(*tensor, &tensor_size));

    // Allocate in Plasma
    std::shared_ptr<arrow::Buffer> out_buf;
    ARROW_RETURN_NOT_OK(client_->Create(oid_out, tensor_size, NULL, 0, &out_buf));

    // Write out (TODO: this copies the buffer, which is redundant
    // because as it is unitialised)
    arrow::io::FixedSizeBufferWriter writer(out_buf);
    int32_t metadata_length; int64_t tensor_size_written;
    ARROW_RETURN_NOT_OK(arrow::ipc::WriteTensor(*tensor, &writer,
						&metadata_length,
						&tensor_size_written));

    // Read back immediately to determine data pointer (TODO: Also
    // likely too complicated. An intermediate hack might be to
    // utilise tensor_size_written under the assumption that there's
    // no footer...)
    arrow::io::BufferReader reader(out_buf);
    ARROW_ASSIGN_OR_RAISE(auto tensor_out, arrow::ipc::ReadTensor(&reader));

    // Construct mutable tensor
    ptrdiff_t offset = tensor_out->raw_data() - out_buf->data();
    int64_t data_size = tensor_out->data()->size();
    assert(offset > 0 && offset < tensor_size); // Make sure it's actually an internal offset
    return arrow::Tensor::Make(type, arrow::SliceMutableBuffer(out_buf, offset, data_size),
			       shape, strides, dim_names);
}

arrow::Status Processor::SealInplaceTensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
						 const std::string &par_name)
{

    // Determine object ID of output
    ARROW_ASSIGN_OR_RAISE(auto oid_out, OIDParameter(batch->GetColumnByName(par_name), par_name));

    // Seal
    ARROW_RETURN_NOT_OK(client_->Seal(oid_out));

    // Release the output object manually. Not sure why this is required...
    return client_->Release(oid_out);
}
