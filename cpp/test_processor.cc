
#include "processor.h"

const std::vector<std::shared_ptr<arrow::Schema> > TEST_PROCS = {
    MakeCallSchema("test", {
	    MakePar("val", arrow::int32(), false, {}),
	    MakeOIDInputPar("inp", false),
	    MakeOIDOutputPar("out", false),
	})
};

class TestProcessor : public Processor {
public:
    TestProcessor() : Processor("TestProcessor", TEST_PROCS) {}

 protected:
    virtual arrow::Status ProcessCall(
        const std::string &proc_func,
        std::shared_ptr<arrow::RecordBatch> batch) override;

};

arrow::Status TestProcessor::ProcessCall(const std::string &proc_func,
                                         std::shared_ptr<arrow::RecordBatch> batch)
{

    // Read parameters
    ARROW_ASSIGN_OR_RAISE(int32_t val,
			  NumericParameter<arrow::Int32Type>(batch, "val"));
    ARROW_ASSIGN_OR_RAISE(auto in_tensor, TensorParameter<arrow::Int32Type>(batch, "inp"));
    const int32_t size = in_tensor->size();

    // "Compute" result tensor
    std::vector<int32_t> out(size);
    const int32_t *in_data =
        reinterpret_cast<const int32_t *>(in_tensor->raw_data());
    for (int i = 0; i < size; i++) {
        out[i] = in_data[i] + val;
    }

    // Write out tensor
    auto out_data = arrow::Buffer::Wrap(out);
    arrow::Tensor out_tensor(arrow::int32(), out_data, {size});
    ARROW_RETURN_NOT_OK(TensorOutput(batch, "out", out_tensor));

    return arrow::Status::OK();
}

int main(int argc, const char* argv[])
{
    return ProcessorMain<TestProcessor>(argc, argv);
}
