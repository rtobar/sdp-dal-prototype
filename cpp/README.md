
Apache Arrow Processing Component Data Exchange Proof-of-concept
================================================================

Motivation
----------

One of the main architectural assumptions of the SKA SDP is that we
cannot rely on storage-based technologies to provide the necessary
performance to handle data exchange between processing
components. These technologies might be sufficient for the data
processing needs as they exist at this point in time, but especially
with technology moving fast towards high-capacity, low-latency stores
it seems like good time to explore how memory could be utilised to
exchange data between execution engines and processing functions.

Design
------

### Physical Data Layout

We will focus on two types of storage objects: Tables and tensors. The
former are assumed to be relatively low in volume and often static
(metadata). The latter are expected to be the representation of bulk
data exchanged by processing components. We will utilise the
[Apache Arrow IPC](https://arrow.apache.org/docs/format/Columnar.html#serialization-and-interprocess-communication-ipc)
format for tables (i.e. each object is a stream of `Schema` followed
by `RecordBatch`es). We similarly use the Apache Arrow tensor format,
which unfortunately isn't currently documented.

### Memory Sharing

For this proof-of-concept we will assume that execution engines and
processing components will be separate processes. This is both to
allow mixing of technologies (e.g. JVM vs. pure C) as well as to allow
easier integration of existing software based around a storage-centric
approach. We will utilise Apache Arrow's
[Plasma In-Memory Object Store](https://arrow.apache.org/docs/python/plasma.html)
to mange memory shared by caller and processor processes.

### Call Data Schema

We will also utilise the Apache Arrow IPC format for inter-process
messaging (i.e. calls). Calls to a particular function are represented
as a table object, with the function name given by metadata associated
with the schema, and parameters given by the columns of the
table. Clearly each row of the table is meant to represent a call to
the function in question.

Parameter references to tables or tensors are represented using Plasma
object IDs and identified using field metadata as "input" or "output"
respectively. It is permitted to add calls with "input" parameters
that have not yet been sealed in the object store, which should result
in them getting delayed until these objects materialise. Equally, a
call is considered complete once all "output" objects have been sealed
in the object store. A call where all outputs already exist in the
object store is considered redundant and should be ignored.

### Messaging

To communicate a call request to a process we also utilise Apache
Plasma using its subscription mechanism. Any process meant to receive
calls must be configured with an object ID prefix that it is meant to
monitor for such call requests.

Building
-----

Version requirements:

	- Arrow 0.16.0 or later
	- Python 3 

Make sure you have Apache Arrow + Plasma installed for C++ and
Python. Check the
[Apache arrow installation page](https://arrow.apache.org/install/)
for details.

Note that the Plasma C++ bindings seem to be distributed separately
from Arrow, so you might for instance have to additionally say:
```
$ sudo yum install -y --enablerepo=epel plasma-devel
```

Now it should be possible to build the test processor:
```
$ make test_processor
```

Test
----

To test the proof-of-concept, start a Plasma server and a processor
and issue a call:
```
$ plasma-store-server -m 10000000 -s /tmp/plasma &
$ ./test_processor /tmp/plasma 00000000 &
$ python caller.py /tmp/plasma 00000000 00000001
```

What should happen is that the caller creates a series of (dependent)
calls in the store and a tensor object to act as the input to the
first call. It then starts waiting for the result of the last call to
materialise:

```
Wrote [2, 2] calls to 0000000000000000000000000000000000000000
Wrote tensor [0, 1, 2, 3, 4, 5, 6, 7, 8, 9] to 0000000100000000000000000000000000000000
Waiting for result in 0000000100000000000000000000000000000004...
```

The processor process will pick up the calls and notice that all the
inputs are missing, and therefore delay their execution at first. Once
the input for the first call arrives, it triggers the first call, the
result of which triggers the second etcetera:

```
Processing 0000000000000000000000000000000000000000
Inputs for scalar_add_tensor missing, delaying.
Inputs for scalar_add_tensor missing, delaying.
Inputs for scalar_add_tensor missing, delaying.
Inputs for scalar_add_tensor missing, delaying.
Processing call to scalar_add_tensor...
Writing to 0000000100000000000000000000000000000001: 1 2 3 4 5 6 7 8 9 10
Processing call to scalar_add_tensor...
Writing to 0000000100000000000000000000000000000002: 3 4 5 6 7 8 9 10 11 12
Processing call to scalar_add_tensor...
Writing to 0000000100000000000000000000000000000003: 4 5 6 7 8 9 10 11 12 13
Processing call to scalar_add_tensor...
Writing to 0000000100000000000000000000000000000004: 6 7 8 9 10 11 12 13 14 15
```

Finally, the caller process will pick up the result tensor:
```
Received: [ 6  7  8  9 10 11 12 13 14 15]
```

Note you can repeat the invocation of `caller.py`, as the objects in
the store get cleaned up after execution.
