#pragma once

#include <memory>
#include <sstream>
#include <iostream>

#include <arrow/ipc/reader.h>
#include <arrow/status.h>
#include <arrow/array.h>
#include <arrow/tensor.h>
#include <arrow/io/memory.h>
#include <arrow/util/logging.h>
#include <arrow/util/key_value_metadata.h>
#include <plasma/client.h>

/// Data type of an Plasma object ID
const auto OBJECT_ID_TYPE = arrow::fixed_size_binary(plasma::kUniqueIDSize);

/// Small helper to parse a partial ObjectID given as a hexadecimal
/// string. Returns the length of the object ID.
int ParseHexObjectID(const std::string &str, plasma::ObjectID *prefix);

/// Processor base class
///
/// A processor connects to a Plasma store and waits for record
/// batches with a certain ObjectID prefix to appear. Each row in such
/// tables is interpreted as calls to the processor. The function to
/// call is given by metadata attached to the table's schema.
///
/// If a table column is itself an object ID and is marked with
/// appropriate meta dta it will be interpreted as references to
/// Plasma objects. If it is marked as "in" this means that the call
/// will only be made once the object in question is created (and
/// sealed) in the Plasma store. If it is marked as "out" this means
/// that the call will be suppressed if the object in question already
/// exists in the object store.
class Processor
{
public:
    Processor(const char *name,
	      const std::vector<std::shared_ptr<arrow::Schema> > &procs)
	: name_(name), procs_(procs) {}
    virtual ~Processor();
    ARROW_DISALLOW_COPY_AND_ASSIGN(Processor);

    /// Connect to Plasma store for calls with given prefix
    arrow::Status Connect(const std::string &plasma_path);

    /// Wait for a process record to appear and process it
    arrow::Status Process();

    /// Disconnect from the Plasma store
    arrow::Status Disconnect() {
        auto status = client_->Disconnect();
        client_.reset();
        obj_table_.clear();
        return status;
    }

protected:

    /// Read a specific value from the given record batch (assumed to only have one row)
    template<typename T, typename Elem>
    static arrow::Result<Elem>
    Parameter(std::shared_ptr<arrow::Array> arr, const std::string &par_name)
    {
        ARROW_RETURN_IF(!arr, arrow::Status::IndexError("Field '", par_name, "' not found!"));
        auto arr_ = std::dynamic_pointer_cast<
            typename arrow::TypeTraits<T>::ArrayType>(arr);
        ARROW_RETURN_IF(!arr_,
                        arrow::Status::TypeError("Wrong array type for '", par_name, "', expected ",
                                                 T::type_name(), ", got ", arr->type()->ToString()));
        ARROW_RETURN_IF(arr->IsNull(0), arrow::Status::IndexError("Field '" + par_name + "' is null!"));

        return std::move(arr_->GetView(0));
    }

    static arrow::Result<plasma::ObjectID> OIDParameter(std::shared_ptr<arrow::Array> arr,
                                                        const std::string &par_name);

    template <typename TYPE>
    static arrow::Result<typename TYPE::c_type>
    NumericParameter(std::shared_ptr<arrow::RecordBatch> batch,
                     const std::string &par_name)
    {
        return Parameter<TYPE, typename TYPE::c_type>(batch->GetColumnByName(par_name),
                                                      par_name);
    }

    /// Get object ID parameter, retrieve Plasma object buffer
    arrow::Result<std::shared_ptr<arrow::Buffer> >
    ObjectParameter(std::shared_ptr<arrow::RecordBatch> batch,
                    const std::string &par_name);

    /// Get object ID parameter, read tensor from Plasma
    arrow::Result<std::shared_ptr<arrow::Tensor> >
    AnyTensorParameter(std::shared_ptr<arrow::RecordBatch> batch,
                       const std::string &par_name);

    /// Get object ID parameter, read tensor from Plasma
    template<typename TYPE>
    arrow::Result<std::shared_ptr<arrow::NumericTensor<TYPE> > >
    TensorParameter(std::shared_ptr<arrow::RecordBatch> batch,
                    const std::string &par_name)
    {

        // Read tensor
        ARROW_ASSIGN_OR_RAISE(auto tensor, AnyTensorParameter(batch, par_name));
        return ToNumericTensor<TYPE>(tensor);
    }

    /// Convert to a numeric tensor of known type (otherwise raises TypeError)
    template<typename TYPE>
    arrow::Result<std::shared_ptr<arrow::NumericTensor<TYPE> > >
    ToNumericTensor(std::shared_ptr<arrow::Tensor> tensor)
    {
        // Make sure the type matches
        arrow::Type::type tid = arrow::TypeTraits<TYPE>::type_singleton()->id();
        ARROW_RETURN_IF(tensor->type_id() != tid,
                        arrow::Status::TypeError("Wrong tensor type, expected '", TYPE::type_name(),
                                                 "' but got '", tensor->type()->name() + "'!"));

        // Convert
        return arrow::NumericTensor<TYPE>::Make(tensor->data(), tensor->shape(),
                                                tensor->strides(),
                                                tensor->dim_names());
    }

    /// Replaces IndexError results with given default value
    template <typename T>
    arrow::Result<T> Optional(arrow::Result<T> maybe_result, T def)
    {
        if (maybe_result.status().IsIndexError())
            return def;
        return maybe_result;
    }

    /// Write tensor as result for output parameter
    arrow::Status TensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
                               const std::string &par_name,
                               const arrow::Tensor &tensor);

    /// Allocate space for tensor output. Use SealInplaceTensorOutput() to finalise.
    arrow::Result<std::shared_ptr<arrow::Tensor> >
    InplaceAnyTensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
                           const std::string &par_name,
                           const std::shared_ptr<arrow::FixedWidthType>& type,
                           const std::vector<int64_t>& shape,
                           const std::vector<int64_t>& strides = {},
                           const std::vector<std::string>& dim_names = {});

    /// Allocate space for tensor output. Use SealInplaceTensorOutput() to finalise.
    template <typename TYPE>
    arrow::Result<std::shared_ptr<arrow::NumericTensor<TYPE> > >
    InplaceTensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
                        const std::string &par_name,
                        const std::vector<int64_t>& shape,
                        const std::vector<int64_t>& strides = {},
                        const std::vector<std::string>& dim_names = {})
    {

        // Make tensor of required type
        auto type = std::static_pointer_cast<arrow::FixedWidthType>(arrow::TypeTraits<TYPE>::type_singleton());
        ARROW_ASSIGN_OR_RAISE(auto tensor, InplaceAnyTensorOutput(batch, par_name, type,
                                                                  shape, strides, dim_names));
        // Convert
        return arrow::NumericTensor<TYPE>::Make(tensor->data(), tensor->shape(),
                                                tensor->strides(), tensor->dim_names());
    }

    /// Seals a tensor that was created in-place in the Plasma store
    arrow::Status SealInplaceTensorOutput(std::shared_ptr<arrow::RecordBatch> batch,
                                          const std::string &par_name);

    /// Process a particular call. Override in derived classes.
    virtual arrow::Status ProcessCall(const std::string &proc_func,
                                      std::shared_ptr<arrow::RecordBatch> batch) = 0;

    // Client connection to Plasma
    std::shared_ptr<plasma::PlasmaClient> client_;

private:

    /// Read calls from given ID in Plasma
    arrow::Status ReadCalls(const plasma::ObjectID &object_id);

    /// Process single call from a record batch
    arrow::Status ReadCall(const plasma::ObjectID &call_oid,
                           std::shared_ptr<arrow::RecordBatch> batch,
                           const std::vector<int> &in_obj_cols,
                           const std::vector<int> &out_obj_cols);

    /// Extract input and output object parameters from schema
    static void FindInOutPars(std::shared_ptr<arrow::Schema> schema,
                              std::vector<int> *in_obj_cols,
                              std::vector<int> *out_obj_cols);

    // Name and supported procs
    std::string name_;
    std::vector<std::shared_ptr<arrow::Schema> > procs_;
    // Namespace root buffer
    std::shared_ptr<arrow::Buffer> root_;
    // Prefix of processing requests addressed to us
    plasma::ObjectID prefix_;
    int prefix_length_;
    // File descriptor subscribed to object updates
    int sub_fd_;
    // Known objects in Plasma.
    plasma::ObjectTable obj_table_;
    // Calls waiting for inputs (input oid -> call oid, record batch)
    using DelayedCall = std::pair<plasma::ObjectID,
                                  std::shared_ptr<arrow::RecordBatch> >;
    std::unordered_multimap<plasma::ObjectID, DelayedCall> delayed_calls_;
};

/// Meta data names
const std::string proc_func_meta = "proc:func";
const std::string proc_par_meta = "proc:par";
const std::string proc_par_meta_in = "in";
const std::string proc_par_meta_out = "out";

/// Meta-data to mark fields as "input"
const auto in_field = std::make_shared<arrow::KeyValueMetadata>(
    std::vector{proc_par_meta}, std::vector{proc_par_meta_in});
/// Meta-data to mark fields as "output"
const auto out_field = std::make_shared<arrow::KeyValueMetadata>(
    std::vector{proc_par_meta}, std::vector{proc_par_meta_out});

/// Create parameter declaration to pass to MakeCallSchema()
inline std::shared_ptr<arrow::Field> MakePar(const std::string &name,
					     std::shared_ptr<arrow::DataType> type,
					     bool nullable=false,
					     std::shared_ptr<const arrow::KeyValueMetadata> metadata=nullptr)
{
    return arrow::field(name, type, nullable, metadata);
}

/// Create Object ID parameter declaration to pass to MakeCallSchema()
inline std::shared_ptr<arrow::Field> MakeOIDPar(const std::string &name, bool nullable=false,
                                                std::shared_ptr<const arrow::KeyValueMetadata> metadata = nullptr)
{
    return MakePar(name, OBJECT_ID_TYPE, nullable, metadata);
}

/// Create input Object ID parameter declaration to pass to MakeCallSchema()
inline std::shared_ptr<arrow::Field> MakeOIDInputPar(const std::string &name, bool nullable=false,
                                                     std::shared_ptr<const arrow::KeyValueMetadata> metadata = nullptr)
{
    static const std::shared_ptr<const arrow::KeyValueMetadata> in_metadata =
        arrow::key_value_metadata({proc_par_meta}, {proc_par_meta_in});
    return MakePar(name, OBJECT_ID_TYPE, nullable,
                   metadata ? metadata->Merge(*in_metadata) : in_metadata);
}

/// Create input Object ID parameter declaration to pass to MakeCallSchema()
inline std::shared_ptr<arrow::Field> MakeOIDOutputPar(const std::string &name, bool nullable=false,
                                                      std::shared_ptr<const arrow::KeyValueMetadata> metadata = nullptr)
{
    static const std::shared_ptr<const arrow::KeyValueMetadata> out_metadata =
        arrow::key_value_metadata({proc_par_meta}, {proc_par_meta_out});
    return MakePar(name, OBJECT_ID_TYPE, nullable,
                   metadata ? metadata->Merge(*out_metadata) : out_metadata);
}

/// Make call schema from a function name and a list of parameters
inline std::shared_ptr<arrow::Schema> MakeCallSchema(const std::string &func_name,
                                                     const std::vector<std::shared_ptr<arrow::Field> > &pars,
                                                     std::shared_ptr<const arrow::KeyValueMetadata> metadata = nullptr)
{
    auto meta = (metadata ? metadata->Copy() : std::make_shared<arrow::KeyValueMetadata>());
    meta->Append(proc_func_meta, func_name);
    return arrow::schema(pars, meta);
}

/// Bytes of the Object ID we are going to use as namespace prefix
const int64_t NAMESPACE_ID_SIZE = 4;
/// Metadata entry for namespace name
const std::string PROC_NAMESPACE_META = "proc:namespace";
/// Metadata entry for namespace process ID
const std::string PROC_NAMESPACE_PID_META = "proc:pid";
/// Metadata entry for namespace process arguments
const std::string PROC_NAMESPACE_ARGV_META = "proc:argv";

/// Checks whether the given object ID declares a namespace.
inline bool IsNamespaceDecl(plasma::ObjectID oid) {
    const uint8_t *d = oid.data();
    for (int i = NAMESPACE_ID_SIZE; i < plasma::kUniqueIDSize; i++)
        if (d[i]) return false;
    return true;
}

/// Schema used for declaring calls valid for namespace
const std::shared_ptr<arrow::Schema> PROCS_SCHEMA =
    arrow::schema({ arrow::field("name", arrow::utf8()),
                    arrow::field("schema", arrow::binary()) });

/// Reserve a new namespace within the Plasma store
arrow::Result<std::pair<plasma::ObjectID, std::shared_ptr<arrow::Buffer> > >
ReserveNamespace(std::shared_ptr<plasma::PlasmaClient> client,
                 const std::string &name,
                 const std::vector<std::shared_ptr<arrow::Schema> > &procs = {},
                 const plasma::ObjectTable *obj_table = nullptr);

template <class Proc>
int ProcessorMain(int argc, const char **argv)
{

    // Make sure we get Plasma path
    if (argc != 2) {
        std::cerr
            << "Please provide plasma socket path!" << std::endl
            << "  Example: " << argv[0] << " /tmp/plasma" << std::endl;
        return 1;
    }

    Proc proc;
    ARROW_CHECK_OK(proc.Connect(argv[1]));

    while(true) {
        Status status = proc.Process();
	if (!status.ok()) {
	    std::cerr << "Processing failed: " << status.ToString() << std::endl;
	}
    }
    // Disconnect the client.
    ARROW_CHECK_OK(proc.Disconnect());

    return 0;
}
