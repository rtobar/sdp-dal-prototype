
from . import common

import pyarrow.plasma as plasma
import pyarrow

import abc
from typing import Tuple, List, Any
import select
import traceback

class Processor(metaclass = abc.ABCMeta):

    def __init__(self, procs:List[pyarrow.Schema],
                 plasma_path:str, prefix:bytes=b'', name:str=None):

        # Create connection
        self._client = plasma.connect(plasma_path)

        # Subscribe to event updates
        self._client.subscribe()
        self._socket = self._client.get_notification_socket()

        # Get list of objects
        self._obj_table = self._client.list()

        # Reserve a namespace
        if name is None:
            name = self.__class__.__name__
        self._prefix, self._root = \
            common.reserve_namespace(self._client, name, procs, self._obj_table)
        print(f"{name} waiting for calls at prefix {common.object_id_hex(self._prefix)}")

        # Initialise
        self._delayed = {}

    def process(self, timeout=None):
        """ Process a call. Blocks if none is currently available.

        :return: False if timeout expired, otherwise True"""

        # Get next notification. This will block. Using select,
        # this way we can implement a timeout.
        rs, _, _ = select.select([self._socket.fileno()], [], [], timeout)
        if len(rs) != 1:
            return False
        oid, data_size, metadata_size = self._client.get_next_notification()

        # Deletion?
        if data_size < 0:
            if oid in self._obj_table:
                del self._obj_table[oid]
            return True

        # Set in object table
        self._obj_table[oid] = {
            'data_size': data_size,
            'metadata_size': metadata_size
        }

        # Has call prefix? Request and attempt to process
        if oid.binary().startswith(self._prefix):
            if not common.is_namespace_decl(oid):
               self._read_calls(oid)

        # Delayed call waiting for this input?
        if oid in self._delayed:
            for call_oid, batch in self._delayed[oid]:
                in_obj_cols, out_obj_cols = self._find_in_out_pars(batch.schema)
                self._read_call(call_oid, batch, in_obj_cols, out_obj_cols)
            del self._delayed[oid]
        return True

    def _read_calls(self, oid:plasma.ObjectID):

        # Receive objects
        object_buffers = self._client.get_buffers([oid], 0)
        if object_buffers[0] is None:
            return

        # Read as a record batch
        batch_reader = pyarrow.RecordBatchStreamReader(
            pyarrow.BufferReader(object_buffers[0]))
        if batch_reader.schema.metadata is None or \
           common.PROC_FUNC_META not in batch_reader.schema.metadata:
            return

        # Identify in+out parameters
        in_obj_cols, out_obj_cols = self._find_in_out_pars(batch_reader.schema)
        proc_func = batch_reader.schema.metadata[common.PROC_FUNC_META].decode('utf8')

        # Get batches
        num_calls = 0
        for batch in batch_reader:

            # Go through rows
            num_calls += batch.num_rows
            for row in range(batch.num_rows):
                batch_row = batch.slice(row, 1)
                self._read_call(oid, batch_row, in_obj_cols, out_obj_cols)

        #print("Received {} calls to {} in {}".format(
        #    num_calls, proc_func, common.object_id_hex(oid)))

    def _find_in_out_pars(self, schema:pyarrow.Schema) \
            -> Tuple[List[int],List[int]]:
        """ Identify input and output parameters in a schema.
        """

        in_obj_cols = []; out_obj_cols = []
        for col in range(len(schema.names)):
            field = schema.field(col)

            # Check that field has ObjectID type and has parametr kind
            # metadata associated with it
            parkind = common.par_meta(field)
            if parkind is None or \
               not field.type.equals(common.OBJECT_ID_TYPE):
                continue

            # Check the concrete parameter kind
            if parkind == common.PROC_PAR_META_IN:
                in_obj_cols.append(col)
            elif parkind == common.PROC_PAR_META_OUT:
                out_obj_cols.append(col)
        return in_obj_cols, out_obj_cols

    def _read_call(self, call_oid:plasma.ObjectID,
                   batch:pyarrow.RecordBatch,
                   in_obj_cols:List[int], out_obj_cols:List[int]):

        # Get function name
        proc_func = batch.schema.metadata[common.PROC_FUNC_META].decode('utf8')

        # Check whether some outputs are not satisfied yet (i.e. the
        # request has not been processed already)
        output_oids = []
        sealed_oids = []
        for col in out_obj_cols:
            oid = plasma.ObjectID(batch.column(col)[0].as_py())
            if oid not in self._obj_table:
                output_oids.append(oid)
                already_processed = False
            else:
                sealed_oids.append(oid)
        if not output_oids:
            print("Outputs {} for {} already sealed, ignoring".format(sealed_oids, proc_func))
            return

        # Check that inputs are present
        for col in in_obj_cols:
            oid = batch.column(col)[0].as_py()
            if oid is None:
                continue
            oid = plasma.ObjectID(oid)
            if oid not in self._obj_table:
                #print("Input {} for {} missing delaying".format(
                #    common.object_id_hex(oid), proc_func))
                if oid in self._delayed:
                    self._delayed[oid].append((call_oid, batch))
                else:
                    self._delayed[oid] = [(call_oid, batch)]
                return

        # Process
        #print("Processing call to {} from {}".format(
        #    proc_func, common.object_id_hex(call_oid)))
        try:
            self._process_call(proc_func, batch)
        except Exception as e:
            print(f"While processing call {call_oid} to {proc_func}:")
            traceback.print_exc()
            return

        # Put placeholders for outputs into object map in case there
        # are other calls writing the same objects
        for oid in output_oids:
            self._obj_table[oid] = ()

    def parameter(self, batch:pyarrow.RecordBatch, name:str,
                  typ:pyarrow.DataType=None, allow_null:bool=False) -> Any:
        """Extract parameter from first row of record batch

        :param batch: Record batch containing parameter
        :param name: Name of parameter to extract
        :param typ: Type to check (optional)
        :param allow_null: Value allowed to be null - will return None
        """
        # Look up column name
        col = batch.schema.get_field_index(name)
        # To type check
        if typ is not None and not batch.schema.field(col).type.equals(typ):
            raise TypeError("Field {} has type {}, expected {}!".format(
                name, batch.schema.field(col).type, typ))
        # Get value, convert to Python interpretation
        val = batch[col][0].as_py()
        if val is None:
            if allow_null:
                return val
            raise ValueError("Field {} of type {} is null!".format(name, typ))
        return val

    def oid_parameter(self, batch:pyarrow.RecordBatch,
                      name:str, allow_null:bool=False) \
            -> plasma.ObjectID:
        """Extract Object ID parameter from first row of record batch.

        :param batch: Record batch containing parameter
        :param name: Name of parameter to extract
        :param allow_null: Value allowed to be null - will return None
        """
        oid = self.parameter(batch, name, common.OBJECT_ID_TYPE, allow_null)
        if oid is None:
            return None
        return plasma.ObjectID(oid)

    def tensor_parameters(self, batch:pyarrow.RecordBatch,
                          tensor_specs:List[Tuple[str,pyarrow.DataType,
                                                  List[str],bool]]) \
            -> List[pyarrow.Tensor]:
        """Read tensors referred to via object ID parameters.

        :param batch: Record batch containing parameters
        :param tensor_specs: Either list of strings or list of tuples
          of form (name, type, dimensionality, allow_null). If given,
          type and dimensionality will be checked. If allow_null is set,
          the object ID is allowed to be null, in which case None will
          get returned instead of a tensor.
        """

        # Collect object IDs
        oid_map = {}
        for i, tensor_spec in enumerate(tensor_specs):
            if isinstance(tensor_spec, tuple):
                name = tensor_spec[0]
                allow_null = (False if len(tensor_spec) < 4 else tensor_spec[3])
            else:
                name = tensor_spec
                allow_null = False
            # Get object ID, skip if None (it was null)
            oid = self.oid_parameter(batch, name, allow_null)
            if oid is None:
                continue
            oid_map[name] = oid
            # Make sure it was marked as an input parameter (because
            # of the previous check this virtually guarantees that the
            # call below will not block)
            if common.par_meta(batch.schema.field(name)) != common.PROC_PAR_META_IN:
                raise ValueError("Tensor call parameter {} not marked "+
                                 "as input!".format(name))

        # Batch-request buffers
        oids = list(oid_map.values())
        bufs = self._client.get_buffers(oids, 100)
        buf_map = { name : buf for name, buf in zip(oid_map.keys(), bufs) }

        # Read them all
        arrays = []
        for tensor_spec in tensor_specs:

            # Determine name and do null check
            pars = []
            if not isinstance(tensor_spec, tuple):
                name = buf_map[tensor_spec]
            else:
                name = tensor_spec[0]
                pars = tensor_spec[1:3]

                # Check for null
                if name not in buf_map and \
                   len(tensor_spec) >= 4 and \
                   tensor_spec[3]:
                    arrays.append(None)
                    continue

            # Result not found in Plasma? Can happen on races when the
            # caller goes away, raise an error
            if buf_map[name] is None:
                raise ValueError(f"Object {oid_map[name]} for parameter {name} vanished from Plasma store!")

            # Get parameter
            arrays.append(common._tensor_from_buf(buf_map[name], name, *pars))

        return arrays

    def tensor_parameter(self, batch:pyarrow.RecordBatch, name:str,
                         typ:pyarrow.DataType=None, dim_names:List[str]=None,
                         allow_null:bool=False) \
            -> pyarrow.Tensor:
        """Read tensor referred to via object ID parameter.

        :param batch: Record batch containing parameters
        :param name: Name of parameter to extract
        :param typ: Tensor value type to check (optional)
        :param dim_names: Tensor dimensionality to check (optional)
        """
        return self.tensor_parameters(batch, [(name, typ, dim_names, allow_null)])[0]

    @abc.abstractmethod
    def _process_call(self, proc_func:str, batch:pyarrow.RecordBatch):
        pass

    def output_tensor(self, batch, name, array, typ=None):
        """Write output tensor to storage

        Note that this is less efficient than constructing it
        in-place, which we should support at some point (TODO)

        :param batch: Record batch containing parameters
        :param name: Name of parameter to extract
        :param arr: Tensor as numpy array
        :param typ: Tensor value type
        """

        # Get object ID
        out_oid = self.oid_parameter(batch, name)

        # Make sure it was marked as an output parameter
        if common.par_meta(batch.schema.field(name)) != common.PROC_PAR_META_OUT:
            raise ValueError("Tensor call parameter {} not marked "+
                             "as output!".format(name))

        # Write tensor
        return common._put_numpy(self._client, out_oid, array, typ, temporary=False)

class LogProcessor(Processor):
    """ Simple processor that just logs all calls. """

    def _process_call(self, proc_func:str, batch:pyarrow.RecordBatch):

        field_strs = []
        for col in range(len(batch.schema.names)):
            field = batch.schema.field(col)
            metadata_str = ''
            if field.metadata is not None:
                metadata_str = dict(field.metadata)
            field_strs.append( "{}={} {}".format(
                field.name, batch.column(col)[0], metadata_str))
        print("{}({})".format(proc_func, ",\n\t".join(field_strs)))
