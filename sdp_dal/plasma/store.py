
from . import common

import pyarrow.plasma as plasma
import pyarrow

import numpy
import weakref
from typing import Iterator, Tuple, List
import select
import time

class TensorRef(object):
    """Refers to a tensor in object storage.

    Might not have been created yet - wraps an Object ID and expected
    type information.
    """

    def __init__(self, store,
                 oid :plasma.ObjectID,
                 typ :pyarrow.DataType=None,
                 dim_names :List[str]=None,
                 buf :pyarrow.Buffer=None):
        self._store = store
        self._oid = oid
        self._typ = typ
        self._dim_names = dim_names
        self._buf = buf
        # Added by Caller if this refers to an object that will only
        # get produced if the listed objects stay alive (i.e. there is
        # a call in-flight that is going to create this object). This
        # ensures that the dependencies are not garbage-collected
        # before the call is finished.
        self._dependencies = []

    @property
    def oid(self) -> plasma.ObjectID:
        return self._oid
    @property
    def typ(self) -> pyarrow.DataType:
        return self._typ
    @property
    def dim_names(self) -> List[str]:
        return self._dim_names

    def put(self, arr :numpy.ndarray):
        """ Write the given value into storage. """

        self._buf = common._put_numpy(self._store._client, self._oid, arr, self._typ, self._dim_names)

    def get(self, timeout=None):
        """ Retrieve the tensor from storage. Might block. """

        # Get buffer, if necessary
        if self._buf is None:

            # Wait for object to become available
            start = time.time()
            while self._oid not in self._store._obj_table:
                if timeout is not None:
                    dt = start + timeout - time.time()
                    if dt <= 0:
                        return None
                    self._store._update_obj_table(dt)
                else:
                    self._store._update_obj_table()

            # Read buffer + make temporary
            self._buf = self._store._client.get_buffers([self._oid], 0)[0]
            self._store._client.delete([self._oid])

            # Failed?
            if self._buf is None:
                raise RuntimeError(f"Object with ID {common.object_id_hex(self._oid)} vanished before it could be read!")

            # Clear dependencies
            self._dependencies = []

        # Read
        return common._tensor_from_buf(self._buf, common.object_id_hex(self._oid),
                                       self._typ, self._dim_names)

class Store(object):

    def __init__(self,
                 plasma_path:str,
                 max_attempts:int=10000,
                 name:str=None):

        # Connect to Plasma
        self._client = plasma.connect(plasma_path)

        # Subscribe to event updates
        self._client.subscribe()
        self._socket = self._client.get_notification_socket()

        # Get list of objects
        self._obj_table = self._client.list()
        self._ns_table_meta = {}
        self._ns_table_procs = {}
        for oid in self._obj_table:
            if common.is_namespace_decl(oid):
                self._parse_namespace(oid)

        # Reserve namespace
        if name is None:
            name = self.__class__.__name__
        self._prefix, self._root = \
            common.reserve_namespace(self._client, name, [], self._obj_table)
        print(f"Store using prefix {common.object_id_hex(self._prefix)} for objects")

        # Request deletion of all objects in our namespace
        for oid in self._obj_table:
            if oid.binary().startswith(self._prefix):
                self._client.delete(oid)

        # Initialise
        self._oid_generator = common.objectid_generator(self._prefix)
        next(self._oid_generator)
        self._max_attempts = max_attempts

    def _update_obj_table(self, timeout :float=0):

        while True:

            # Get next notification. This will block. Using select,
            # this way we can implement a timeout.
            rs, _, _ = select.select([self._socket.fileno()], [], [], timeout)
            if len(rs) == 0:
                return
            oid, data_size, metadata_size = self._client.get_next_notification()

            # Deleted?
            if data_size < 0 or metadata_size < 0:
                if oid not in self._obj_table:
                    continue
                del self._obj_table[oid]
                if common.is_namespace_decl(oid):
                    del self._ns_table_meta[oid]
                    del self._ns_table_procs[oid]
                continue

            # Set in object table
            self._obj_table[oid] = {
                'data_size': data_size,
                'metadata_size': metadata_size
            }

            # Namespace?
            if common.is_namespace_decl(oid):
                try:
                    self._parse_namespace(oid)
                except Exception as e:
                    # Ignore anything going wrong here, we don't want
                    # this to break other code
                    print("Failed to parse namespace: ", e)

            # Reset timeout (process any remaining notifications)
            timeout = 0

    def _parse_namespace(self, oid : plasma.ObjectID):

        # Get buffer, copy. We do not want to leave behind trailing
        # references to namespace objects.
        buf = pyarrow.py_buffer(self._client.get_buffers([oid], 0)[0])
        if buf is None:
            # vanished before we got to it, simply ignore
            return
        procs = {}
        meta = {}
        if buf.size > 0:

            # Parse
            reader = pyarrow.ipc.open_stream(pyarrow.BufferReader(buf))
            if reader.schema.metadata is not None:
                meta = reader.schema.metadata

            # Read supported calls
            name_col = reader.schema.get_field_index('name')
            schema_col = reader.schema.get_field_index('schema')
            for batch in reader:
                iters = [ iter(col) for col in batch.columns ]
                for i in range(batch.num_rows):
                    name = next(iters[name_col]).as_py()
                    schema_buf = next(iters[schema_col]).as_buffer()
                    # Attempt to read schema. We can use a record batch reader
                    # for this - a serialised schema looks just like a record
                    # batch stream without any actual record batches.
                    call_schema = pyarrow.ipc.open_stream(pyarrow.BufferReader(schema_buf)).schema
                    procs[name] = call_schema

        # Save
        self._ns_table_meta[oid] = meta
        self._ns_table_procs[oid] = procs

    def new_tensor_ref(self, typ :pyarrow.DataType = None,
                       dim_names :List[str] = None) -> TensorRef:
        """ Allocate an Object ID for a new tensor in Plasma

        :param typ: Element datatype. If `ComplexType`, will convert.
        :param dim_names: Dimension names
        :returns: Reference to tensor
        """

        for _ in range(self._max_attempts):

            # Create a new OID
            oid = next(self._oid_generator)

            # Check whether it is free. Note this is not race-safe -
            # we *are* basically assuming that there's no two
            # processes using the same namespace for storage at the
            # same time. However this might help when recovering from
            # a crash.
            self._update_obj_table()
            if oid not in self._obj_table:
                return TensorRef(self, plasma.ObjectID(oid), typ, dim_names)

        raise RuntimeError("Maximum number of retries reached while " +
                           "attempting to find unused object ID for tensor!")

    def put_new_tensor(self, arr :numpy.ndarray,
                       typ :pyarrow.DataType = None,
                       dim_names :List[str] = None) -> TensorRef:
        """ Allocate and create a new tensor in Plasma

        :param arr: Data as numpy array
        :param typ: Element datatype. If ComplexType, will convert.
        :param dim_names: Dimension names
        :returns: Reference to tensor
        """

        # Allocate in Plasma
        ref = self.new_tensor_ref(typ, dim_names)

        # Put, return
        ref.put(arr)
        return ref
